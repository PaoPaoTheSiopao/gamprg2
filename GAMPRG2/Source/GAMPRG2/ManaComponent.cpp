// Fill out your copyright notice in the Description page of Project Settings.

#include "ManaComponent.h"

// Sets default values for this component's properties
UManaComponent::UManaComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


void UManaComponent::ManaRegen(float DeltaTime, float Manipulation)
{
	if (CurMana > MaxMana)
	{
		CurMana = MaxMana;
	}
	else if (CurMana < 0)
	{
		CurMana = 0;
	}
	else
	{
		CurMana += (ManaRegeneration + Manipulation)*DeltaTime;
	}
}

// Called when the game starts
void UManaComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UManaComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

