// Fill out your copyright notice in the Description page of Project Settings.

#include "UnitAttributeComponent.h"

// Sets default values for this component's properties
UUnitAttributeComponent::UUnitAttributeComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


void UUnitAttributeComponent::UpdateIntelligenceStats()
{
	SpellDamage = Intelligence * 0.07+(1+SpellAmplify);
}

void UUnitAttributeComponent::UpdateAgilityStats()
{
	MovementSpeed = (BaseMovementSpeed + FlatMovementSpeed)*(1 + MovementSpeedBonuses);
	AttackPerSecond = ((100 + AddedAttackSpeed)*0.1) / BaseAttackSpeed;
	AttackTime = BaseAttackSpeed / ((100 + AddedAttackSpeed)*0.01);

}
/*
float UUnitAttributeComponent::Evasion2()
{
	float butterfly = 0.3f;
	float talismanEva = 0.15f;
	float blind = 0.5f;

	EvasionModifier *= 1 - butterfly;
	EvasionModifier *= 1 - talismanEva;
	EvasionModifier *= 1 - blind;


	return 1 - EvasionModifier;
}*/

void UUnitAttributeComponent::UpdateUnit(float DeltaTime,float ManaManip,float HealthManip)
{
	//UpdateMana(DeltaTime,ManaManip);
	UpdateAgilityStats();
	UpdateIntelligenceStats();
}


void UUnitAttributeComponent::UpdateMana(float DeltaTime, float Manipulation)
{
	/*
	MaxMana = BaseMana + (12 * Intelligence);
	ManaRegeneration = Intelligence * 0.05;
	if (MaxMana > MaxMana)
	{
		CurMana = MaxMana;
	}
	else
	{
		CurMana += (ManaRegeneration-Manipulation) * DeltaTime;
	}*/
}

int32 UUnitAttributeComponent::CheckSourcesOfEvasion( float Manipulation)
{
	int32 sourceInt = 0;
	return sourceInt;
}

float UUnitAttributeComponent::ReturnTargetRotation(FVector user, FVector target)
{
	FVector magnitude = user - target;
	
	float angle = FMath::RadiansToDegrees(FMath::Atan2(magnitude.X,magnitude.Y));
	return angle;
}

float UUnitAttributeComponent::ReturnMagRessistance(float BonMagRessit)
{
	float totalMagRes = 1 - (1 - 0.25)*(1 - BonMagRessit);
	return totalMagRes;
}

int32 UUnitAttributeComponent::UpdateNormalDamage()
{
	switch (MainAttribute)
	{
	case Attributes::Agility:
		return Agility + BonusAgility;
		break;
	case Attributes::Intelligence:
		return Intelligence + BonusIntelligence;
		break;
	case Attributes::Strength:
		return Strength + BonusStrength;
		break;
	default:
		return 0;
		break;
	}
}



// Called when the game starts
void UUnitAttributeComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UUnitAttributeComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

