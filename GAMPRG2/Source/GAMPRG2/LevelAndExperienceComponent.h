// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "LevelAndExperienceComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMPRG2_API ULevelAndExperienceComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	ULevelAndExperienceComponent();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level")
		int32 HeroLvl = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Experience")
		float currentExperience = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Experience")
		float BaseDeathXp; 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Experience")
		float NeuBaseDeathXp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level")
		int32 AvailableSkillPoints = 1; 

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Experience")
		TArray<int32> ExpReq = {230,600,1080,1660,2260,2980,3730,4510,5320,6160,7030,7930,9155,10405,11680,12980,14305,15805,17395,18995,20845,22945,25295,27895};

	UFUNCTION(BlueprintCallable, Category = "Experience")
		void UpdateNeutralXPGiven();
	UFUNCTION(BlueprintCallable, Category = "Experience")
		void UpdateXp(int32 xpGained); 
	UFUNCTION(BlueprintCallable, Category = "Experience")
		float ComputeExpDistribution(int32 numDis);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
