// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Status.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMPRG2_API UStatus : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UStatus();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status")
		bool effectIsActive;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status")
		float duration;
	UFUNCTION(BlueprintCallable, Category = "Duration")
		void effectExpiration(float DeltaTime);
	UFUNCTION(BlueprintImplementableEvent, Category = "Status Effects")
		void TestEvent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
