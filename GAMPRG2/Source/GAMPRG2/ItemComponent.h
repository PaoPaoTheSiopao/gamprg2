// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ItemComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMPRG2_API UItemComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UItemComponent();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus")
		float IBonusAgility;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus")
		float IBonusStrength;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus")
		float IBonusIntelligence;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus")
		float IBonusArmor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus")
		float IBonusAttackDamage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus")
		float IBonusHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus")
		float IBonusMana;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus")
		float IBonusHealthRegeneration;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus")
		float IBonusManaRegeneration;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus")
		float IBonusMovementSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus")
		float IBonusAttackSpeed;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
