// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TowerComponentClass.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMPRG2_API UTowerComponentClass : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTowerComponentClass();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Health;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Armor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Damage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Range;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float BaseAttackTime;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
