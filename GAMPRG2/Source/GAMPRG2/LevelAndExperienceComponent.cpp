// Fill out your copyright notice in the Description page of Project Settings.

#include "LevelAndExperienceComponent.h"

// Sets default values for this component's properties
ULevelAndExperienceComponent::ULevelAndExperienceComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


void ULevelAndExperienceComponent::UpdateNeutralXPGiven()
{
	float temp = NeuBaseDeathXp * 0.2;
	BaseDeathXp = temp + BaseDeathXp;
}

void ULevelAndExperienceComponent::UpdateXp(int32 xpGained)
{
	if (HeroLvl != 25)
	{
		currentExperience += xpGained;
		for (int32 i = HeroLvl-1; i < ExpReq.Num(); i++)
		{
			if (currentExperience >= ExpReq[i])
			{
				HeroLvl++;
				AvailableSkillPoints++;
			}
		}
	}
}

float ULevelAndExperienceComponent::ComputeExpDistribution(int32 numDis)
{
	float expDis = (BaseDeathXp + 0.14*currentExperience) / numDis;
	return expDis;
}

// Called when the game starts
void ULevelAndExperienceComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void ULevelAndExperienceComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

