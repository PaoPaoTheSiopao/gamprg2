// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMPRG2_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float BaseHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float MaxHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float CurHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float damModifier;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float HealthRegeneration;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mana")
		float BonusHealthRegen;

	UFUNCTION(BlueprintCallable, Category = "Health")
		float TakeDamage(float phyDamage, float magDamage, float pureDamage, float tarArmor, float tarMagRessist);
	UFUNCTION(BlueprintCallable, Category = "Health")
		void HealthRegen(float DeltaTime, float Manipulation);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
