// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerClass.h"

// Sets default values
ATowerClass::ATowerClass()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ATowerClass::DealDamage(int32 targetHealth)
{
	targetHealth -= Damage; 
}

// Called when the game starts or when spawned
void ATowerClass::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATowerClass::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

