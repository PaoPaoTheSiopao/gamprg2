// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DamageComponent.generated.h"

UENUM(BlueprintType)
enum class DamageType : uint8 {
	Physical,
	Magical,
	Pure,
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMPRG2_API UDamageComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UDamageComponent();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		DamageType type;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		int32 BaseNormalDamage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		int32 BonusNormalDamage;




protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
