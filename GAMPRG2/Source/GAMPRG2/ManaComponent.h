// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ManaComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMPRG2_API UManaComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UManaComponent();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mana")
		float BaseMana;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mana")
		float MaxMana;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mana")
		float CurMana;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mana")
		float ManaRegeneration;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mana")
		float BonusManaRegen;

	UFUNCTION(BlueprintCallable, Category = "Mana")
		void ManaRegen(float DeltaTime, float Manipulation);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
