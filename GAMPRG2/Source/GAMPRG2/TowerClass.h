// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TowerClass.generated.h"

UCLASS()
class GAMPRG2_API ATowerClass : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATowerClass();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Health;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Armor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Damage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Range;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float BaseAttackTime;

	UFUNCTION(BlueprintCallable, Category = "Damage")
		void DealDamage(int32 targetHealth);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
