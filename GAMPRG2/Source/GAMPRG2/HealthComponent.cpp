// Fill out your copyright notice in the Description page of Project Settings.

#include "HealthComponent.h"

float UHealthComponent::TakeDamage(float phyDamage, float magDamage, float pureDamage, float tarArmor, float tarMagRessist)
{
	float totalPhyDamage = ((1 - (tarArmor*0.02) / (0.9 + abs(tarArmor)*0.048))*phyDamage);
	float totalMagDamage = magDamage * tarMagRessist;
	float totalDamage = totalPhyDamage + totalMagDamage + pureDamage;
	totalDamage = totalDamage + totalDamage * damModifier;
	return totalDamage;
}

void UHealthComponent::HealthRegen(float DeltaTime, float Manipulation)
{
	HealthRegeneration =  0.1;
	if (CurHealth > MaxHealth)
	{
		CurHealth = MaxHealth;
	}
	else if(CurHealth < 0)
	{
		CurHealth = 0;
	}
	else
	{
		CurHealth += (HealthRegeneration + Manipulation)*DeltaTime;
	}
}

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

