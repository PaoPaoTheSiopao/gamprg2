// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Faction.generated.h"

UENUM(BlueprintType)
enum class Factions : uint8 {
	Dire,
	Radiant,
	Neutral,
};
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMPRG2_API UFaction : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFaction();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Faction")
		Factions FactionSide;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
