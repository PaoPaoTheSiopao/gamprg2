// Fill out your copyright notice in the Description page of Project Settings.

#include "ArmorComponent.h"

// Sets default values for this component's properties
UArmorComponent::UArmorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

float UArmorComponent::GetArmor(float ArmorModi, float agi)
{
	TotalArmor = BaseArmor + ArmorModi + (agi*0.16);
	return TotalArmor;
}

float UArmorComponent::GetArmor2(float ArmorModi, float agility, UUnitAttributeComponent * sl)
{
	return 0.0f;
}

// Called when the game starts
void UArmorComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UArmorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

