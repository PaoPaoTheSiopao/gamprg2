// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ArmorComponent.generated.h"

UENUM(BlueprintType)
enum class ArmorTypes : uint8 {
	Normal,
	Basic,
	Structure,
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMPRG2_API UArmorComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UArmorComponent();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Armor")
		float TotalArmor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Armor")
		float BaseArmor;

	UFUNCTION(BlueprintCallable, Category = "Armor")
		float GetArmor(float ArmorModi, float agility);
	UFUNCTION(BlueprintCallable, Category = "Armor")
	float GetArmor2(float ArmorModi, float agility, class UUnitAttributeComponent* sl);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
