// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UnitAttributeComponent.generated.h"


UENUM(BlueprintType)
enum class Attributes : uint8 {
	Strength,
	Intelligence,
	Agility,
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMPRG2_API UUnitAttributeComponent : public UActorComponent
{
	GENERATED_BODY()

		
public:	
	// Sets default values for this component's properties
	UUnitAttributeComponent();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
		Attributes MainAttribute;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
		int32 Strength;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
		int32 BonusStrength;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
		int32 Agility;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
		int32 BonusAgility;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
		int32 Intelligence;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
		int32 BonusIntelligence;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Strength")
		float MagicResistance;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Strength")
		float BaseMagicResistance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Intelligence")
		float SpellDamage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Intelligence")
		float SpellAmplify;
	UFUNCTION(BlueprintCallable, Category = "Intelligence")
		void UpdateIntelligenceStats();

	
	////////////////////
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Agility")
		float MovementSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Agility")
		float BaseMovementSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Agility")
		float FlatMovementSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Agility")
		float MovementSpeedBonuses;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Agility")
		float AttackSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Agility")
		float AddedAttackSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Agility")
		float BaseAttackSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Agility")
		float AttackPerSecond;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Agility")
		float AttackTime;
	UFUNCTION(BlueprintCallable, Category = "Agility")
		void UpdateAgilityStats();

	//UFUNCTION(BlueprintPure)
	//float EvasionModifier = 1.0f;
	//float Evasion2();


	UFUNCTION(BlueprintCallable, Category = "Unit")
		void UpdateUnit(float DeltaTime, float ManaManip, float HealthManip);
	UFUNCTION(BlueprintCallable, Category = "Mana")
		void UpdateMana(float DeltaTime, float Manipulation);
	int32 CheckSourcesOfEvasion( float Manipulation);


	UFUNCTION(BlueprintCallable, Category = "Mana")
		float ReturnTargetRotation(FVector user, FVector target);



	UFUNCTION(BlueprintCallable, Category = "Intelligence")
		float ReturnMagRessistance(float BonMagRessit);


	UFUNCTION(BlueprintCallable, Category = "Mana")
		int32 UpdateNormalDamage();

	void CheckCollider(class UBoxComponent* collider);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
