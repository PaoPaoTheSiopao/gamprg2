// Fill out your copyright notice in the Description page of Project Settings.

#include "Status.h"

// Sets default values for this component's properties
UStatus::UStatus()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}



void UStatus::effectExpiration(float DeltaTime)
{
	effectIsActive = true;
	if (duration<=0)
	{
		duration = 0;
		effectIsActive = false;
	}
	else
	{
		duration -= 1 * DeltaTime;
	}
}


// Called when the game starts
void UStatus::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UStatus::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

